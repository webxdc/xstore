## WebXDC store: discover and share the latest apps to your chats

Welcome to the WebXDC store (also xdc store for short) which currently consists of:

- **Store bot**: A Rust-implemented bot which operates on an e-mail
  address and sends out the store xdc app ("frontend"). 

- **Store xdc** (frontend): Gets sent from the bot to users when they setup contact
  or send the bot a message. The store xdc supports search and
  discovery of the **app index**, including forwarding/sharing an app into user-chosen chats. 

- The store bot command line interface allows to import apps to 
  make them available in the **app index** which is presented by the store xdc. 

Currently all Bot/Frontend communication happens via Delta Chat messaging. 
The Bot does not need to be internet-reachable. 


## Getting started
To try out the bot, you can send a message to: xstore@testrun.org or scan this qr-code:

<div style="display:flex; justify-content: center">
  <img width="300" height="300" src="assets/1o1_invite_qr.png"></img>
</div>

After establishing initial contact the bot will send a store xdc app which provides
the central point of interaction for users: 

1. Scroll the app index and hit the "share/sendToChat/forward"
   button on any app. 

2. Select the chat where you want to share the app.
   If you just want to test an app yourself select the "Saved Messages" chat. 

3. The app will now appear in the selected chat in draft mode so that
   you can set a text message and send the app to your chat partners.

4. Everyone in the chat can now start the app. The other chat members don't
   need to interact with the store bot at all. 


## Usability notes regarding store bot xdc frontend 

- At the bottom of the store xdc app you can trigger an update of the "app index" 
to make sure you have the latest versions. 

- When hitting the "sendToChat/sendToChat/forward" button on any app for
  the first time a download will be triggered (using the send/receive message WebXDC APIs).

## General development ideas
- The webxdc store runs as a standalone bot so that it can be run by other people for their own needs and on their own e-mail addresses. 

- We offer a community store populated with initial apps
  also to determine what needs to happen with messenger WebXDC APIs and messenger integration. 

- The implementation of the store should be as independent as feasible from Delta Chat specifics so that it could eventually work also for Cheogram, Briar, Monocles or other upcoming messengers supporting WebXDC. 

## Setting up the bot

### Installing pre-built binary releases

Download the latest release from https://codeberg.org/webxdc/store/releases,
extract the compressed archive to the desired location, it containts the bot command line
program and required assets. In the following steps it is assumed that you added the program
to your PATH variable, but you can also run the binary directly.

To check the program is installed correctly run:

```
xstore version
```

You should see the version code printed on the screen.

If you want to install from source code, read [Developing / Building from source](#developing--building-from-source)

> To try out the unreleased development version with the latest state of
> the source code, download the pre-build at:
> https://download.delta.chat/store/preview/xstore-main.tar.gz
> **WARNING:** Don't use the development version in production, use it at your own risk for testing only.

### Running the bot

To run the bot, set the environment variables
`addr` and `mail_pw` with the login and password
and use the command `start`:

```
addr=bot@example.org mail_pw=My_P4ssword xstore start
```

The environment variables need to be set the first time you start the bot
and will be saved into the bot account database afterwards.

You may set the `RUST_LOG=info` environment variable to get detailed logging from the bot.

### Importing apps

To import WebXDC applications into the app index:

```
xstore import /path/to/xdc_apps/
```

where `/path/to/xdc_apps/` is a folder with WebXDC files you want to import.

### Per-app metadata

The store bot uses the following meta data for each xdc app,
noted down in rust-struct style: 

```rust
// Fields specified in https://docs.webxdc.org/spec.html#the-manifesttoml-file
pub name: String,
pub image: String,
pub source_code_url: Option<String>,

// Fields not specified yet but required for store bot purposes 
pub description: String,
pub app_id: String,
pub tag_name: String,
pub date: Date,
```

Notes: 

- The `app_id` and `tag_name` fields are used by the bot to sort submitted apps so that 
  the store xdc can offer the latest version of each app to users.

- The `app_id` field MUST be ASCII-alphanumeric with only `.` allowed as special character. 
  Casing doesn't matter and will be ignored by the bot when doing `app_id` comparisons. 

- We do not define "authorship" yet because it likely is better to
  introduce together with code signing so that the information is authenticated. 
  However, the `source_code_url` already provides an (unauthenticated) 
  way to point to the author(s). 

### Configuring the bot
Upon first startup, the bot will create a template privacy notice in `.config/xdcstore/privacy-notice.html`.
You need to fill out the blanks according to your private information or create your own privacy notice all along.
The first paragraph will be appended to the store-message.
If you don't want to show this privacy notice, you can delete `privacy-notice.html` so it will no longer show up.

### Uninstalling / Resetting Bot

The bot stores its data in the user's home config directory.

The exact location differs depending on the operating system.
```
linux: ~/.config/xstore/
macOS: ~/Library/Application\ Support/xstore/
windows: %AppData%\xstore\config
```

## Developing / building from source

### Requirements

You need to have installed:
- `rust` (https://www.rust-lang.org/tools/install)
- `node` (https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
- `pnpm` (https://pnpm.io/installation)

### Building the frontend

The bot uses WebXDC apps as "frontend" to interact with users.
Go to the `frontend` folder and install dependencies running:

```
pnpm install
```

Build the frontend by running:

```
pnpm build
```

To package the frontend with developer tools inside for debugging:

```
ERUDA=yes pnpm build
```

This step creates an `assets` directory in the root of the repository and moves the bundled frontend (`store.xdc`) there.

### Building the bot

```
cargo build
```

The binary will be available in `./target/debug/` folder.

While developing it is easier to build and run the bot in a single step with:

```
addr=bot@example.org mail_pw=My_P4ssword cargo run -- start
```

### Importing test apps

For testing, there are some apps in the folder `example-xdcs` you can import:

```
cargo run -- import example-xdcs
```

### Building pull requests

The CI automatically builds releases tarballs for testing for every GitHub Pull Request branch, the
download link can be found in the CI checks details section at the bottom of the Pull Request page.

### Testing the frontend

While developing the frontend, run `pnpm dev` in the `frontend` folder to test your changes in a
small emulator with mock data.

Some functions can be tested completely automatically with `pnpm run test`.
The tests are known to work with Node.js v18.17.1, but not with Node.js v20.6.0
due to a [bug in `vite-plugin-solid`](https://github.com/solidjs/vite-plugin-solid/issues/104).

### Running automated tests 

Tests are using [pytest](https://pytest.org/).

To run the tests, first build the bot in debug mode `cargo build` which builds the binary.

To run the tests, you need to install [tox](https://tox.wiki/)
and set `CHATMAIL_DOMAIN=nine.testrun.org` environment variable.

Executing `tox` will run the tests.

To develop the tests, create a development environment with `tox -e py --devenv env`
and activate it with `. env/bin/activate`. Afterwards run `pytest`.

The bot also has some rust tests which can be run like this:
```
cargo test
```

### Releasing

To automatically build and create a new GitHub release:

```
git tag -a v1.0.1
git push origin v1.0.1
```

To create locally the distribution tarball run the script at `./scripts/dist.sh`
