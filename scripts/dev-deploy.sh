#!/usr/bin/env bash
#
# Script to deploy a bot locally.

set -xeuo pipefail

if test $# -gt 1; then
	echo "usage: $0" >&2
	exit 1
fi

if test $# -lt 1; then
	# Deploy to dev/ by default.
	DEPLOY_HOME="$(realpath dev)"
else
	DEPLOY_HOME="$(realpath $1)"
fi
mkdir -p "$DEPLOY_HOME"

SRC="$PWD"

# Build the frontend.
cd "$SRC/frontend"
pnpm install
pnpm build

# Build the backend.
cd "$SRC"
cargo build

# Prepare XDCs.
cd "$SRC/../xdcget"

VENV="$DEPLOY_HOME/venv"

python3 -m venv "$VENV"
"$VENV/bin/pip" install -e .
"$VENV"/bin/xdcget update

# Configure the bot.
cd "$SRC"

configured_addr="$(HOME="$DEPLOY_HOME" target/debug/xstore addr 2>/dev/null || echo)"

if test -z "$configured_addr"; then
	RESPONSE=$(curl -X POST $DCC_NEW_TMP_EMAIL)
	addr=$(echo "$RESPONSE" | python3 -c 'import json,sys;print(json.load(sys.stdin)["email"])')
	mail_pw=$(echo "$RESPONSE" | python3 -c 'import json,sys;print(json.load(sys.stdin)["password"])')

	configured_addr="$(HOME="$DEPLOY_HOME" addr="$addr" mail_pw="$mail_pw" target/debug/xstore addr)"
fi

# Import XDCs into the bot.
HOME="$DEPLOY_HOME" target/debug/xstore import ../xdcget/export/

echo "Starting the bot at address $configured_addr"
qrencode -t UTF8 -- "mailto:$configured_addr"

HOME="$DEPLOY_HOME" target/debug/xstore start
