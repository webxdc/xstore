#!/bin/sh
#
# Script to create the distribution tarball.

set -e

SRC="$PWD"

# Build the frontend.
cd "$SRC/frontend"
pnpm install
pnpm build

# Build the backend.
cd "$SRC"
cargo build --target x86_64-unknown-linux-musl --release

TMP="$(mktemp -d)"
DESTDIR="$TMP/xstore"
mkdir "$DESTDIR"
cp target/x86_64-unknown-linux-musl/release/xstore "$DESTDIR/xstore"

mkdir -p "$SRC/dist"
OUT="$SRC/dist/xstore.tar.gz"
tar -C "$TMP" -czf "$OUT" xstore 

echo Distribution tarball is built at $OUT >&2

rm -fr "$TMP"
