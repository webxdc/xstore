//! Appinfo for the store index.
use crate::{db::RecordId, manifest::get_webxdc_manifest, utils::read_vec};
use anyhow::{Context as _, Result};
use async_zip::tokio::read::fs::ZipFileReader;
use base64::encode;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
use time::{format_description::well_known::Rfc3339, OffsetDateTime};
use tokio::fs::File;
use ts_rs::TS;

/// Information about a single application in the store index.
#[derive(TS, Deserialize, Serialize, Clone, Debug, Default, PartialEq)]
#[ts(export)]
#[ts(export_to = "frontend/src/bindings/")]
pub struct AppInfo {
    #[allow(clippy::missing_docs_in_private_items)]
    #[serde(skip)]
    pub id: RecordId,

    /// Application ID, e.g. `webxdc-poll`.
    pub app_id: String,

    /// Release tag, e.g. `v2.2.0`.
    pub tag_name: String,

    /// Date as a timestamp in seconds.
    pub date: i64,

    /// Application name, e.g. `Checklist`.
    pub name: String,

    /// Source code URL, e.g. `https://codeberg.org/webxdc/checklist`.
    pub source_code_url: String,

    /// Application icon encoded as a data URL,
    /// for example `data:image/png;base64,...`.
    pub image: String,

    /// Human-readable application description.
    pub description: String,

    /// Application size in bytes.
    pub size: i64,

    /// Absolute path to the .xdc file.
    #[serde(skip)]
    pub xdc_blob_path: PathBuf,

    /// True if the application has been removed.
    #[serde(skip)]
    pub removed: bool,
}

impl AppInfo {
    /// Create [AppInfo] from webxdc file.
    pub async fn from_xdc(file: &Path) -> Result<Self> {
        let size = i64::try_from(File::open(&file).await?.metadata().await?.len())?;
        let reader = ZipFileReader::new(&file).await?;
        let entries = reader.file().entries();
        let manifest = get_webxdc_manifest(&reader).await?;

        let image = entries
            .iter()
            .enumerate()
            .map(|(index, entry)| (index, entry.entry().filename().as_str().unwrap_or_default()))
            .find(|(_, name)| *name == "icon.png" || *name == "icon.jpg");
        let image = if let Some((index, name)) = image {
            let res = read_vec(&reader, index).await?;
            let ending = name
                .split('.')
                .nth(1)
                .context(format!("Can't extract file ending from {name}"))?;
            let base64 = encode(&res);
            Ok(format!("data:image/{ending};base64,{base64}"))
        } else {
            Err(anyhow::anyhow!("Could not find image"))
        };

        Ok(Self {
            size,
            date: OffsetDateTime::parse(&manifest.date, &Rfc3339)?.unix_timestamp(),
            app_id: manifest.app_id,
            tag_name: manifest.tag_name,
            name: manifest.name,
            source_code_url: manifest.source_code_url,
            image: image?,
            description: manifest.description,
            xdc_blob_path: file.to_path_buf(),
            id: 0, // This will be updated by the db on insert
            removed: false,
        })
    }
}
