//! Store bot.

#![warn(
    clippy::all,
    clippy::indexing_slicing,
    clippy::unwrap_used,
    clippy::missing_docs_in_private_items,
    missing_docs
)]
mod appinfo;
mod bot;
mod cli;
mod db;
mod import;
mod manifest;
mod messages;
mod store;
mod updates;
mod utils;
use std::path::PathBuf;
use std::{fs::create_dir_all, sync::Arc};

use anyhow::{Context as _, Result};
use bot::Bot;
use build_script_file_gen::include_file_str;
use clap::Parser;
use cli::{BotActions, BotCli};
use deltachat::config::Config;
use deltachat::context::Context;
use deltachat::stock_str::StockStrings;
use deltachat::Events;
use log::info;
use notify::event::{AccessKind, AccessMode};
use notify::{Event, RecursiveMode, Watcher};
use tokio::signal;
use tokio::sync::mpsc;
use utils::{project_dirs, AddType};

/// File name of the setup contact QR code.
const INVITE_QR: &str = "1o1_invite_qr.png";

/// Bot version printed in response to the `version` command line command
/// and sent back in response to the `/version` chat message.
const VERSION: &str = include_file_str!("VERSION");

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info"))
        .format_timestamp(None)
        .init();
    let cli = BotCli::parse();

    match &cli.action {
        BotActions::Import { path, watch } => {
            let path = PathBuf::from(path);
            let bot = Bot::new().await.context("Failed to create bot")?;
            let xdcs_dir = project_dirs()?.config_dir().to_path_buf().join("xdcs");
            create_dir_all(&xdcs_dir)?;

            if path.is_file() {
                match import::import_one(
                    path.as_path(),
                    &xdcs_dir,
                    &mut *bot.get_db_connection().await?,
                )
                .await?
                {
                    AddType::Added => println!("Added {}", path.display()),
                    AddType::Updated => println!("Updated {}", path.display()),
                    AddType::Ignored => println!("Ignored {}", path.display()),
                }
            } else if path.is_dir() {
                if *watch {
                    info!("Watching {} for changes", path.join("xdget.lock").display());
                    let (tx, mut rx) = mpsc::channel(5);
                    let mut watcher = {
                        notify::recommended_watcher(move |res: notify::Result<Event>| match res {
                            Ok(event) => {
                                match event.kind {
                                    notify::EventKind::Create(_)
                                    | notify::EventKind::Access(AccessKind::Close(
                                        AccessMode::Write,
                                    )) => {
                                        if let Err(e) = tx.blocking_send(()) {
                                            eprintln!("Channel error: {:?}", e);
                                        }
                                    }
                                    _ => (),
                                };
                            }
                            Err(e) => println!("Watch error: {:?}", e),
                        })?
                    };
                    watcher.watch(path.as_path(), RecursiveMode::NonRecursive)?;
                    let dest = Arc::new(xdcs_dir);
                    let path = Arc::new(path.clone());
                    let bot = Arc::new(bot);
                    tokio::spawn(async move {
                        loop {
                            rx.recv().await;
                            println!("Updating xdcs...");
                            if let Err(e) = async {
                                import::import_many(
                                    path.as_path(),
                                    dest.as_path(),
                                    &mut *bot.get_db_connection().await?,
                                )
                                .await
                            }
                            .await
                            {
                                eprintln!("Import error: {:?}", e);
                            } else {
                                println!()
                            }
                        }
                    });
                    signal::ctrl_c().await?;
                } else {
                    import::import_many(
                        path.as_path(),
                        xdcs_dir.as_path(),
                        &mut *bot.get_db_connection().await?,
                    )
                    .await?;
                }
            } else {
                eprintln!("{} is not a file or directory", path.display());
            }
        }
        BotActions::Addr => {
            let dirs = project_dirs()?;
            std::fs::create_dir_all(dirs.config_dir())?;
            let deltachat_db_file = dirs.config_dir().to_path_buf().join("deltachat.db");
            let context = Context::new(
                deltachat_db_file.as_path(),
                1,
                Events::new(),
                StockStrings::new(),
            )
            .await
            .context("Failed to create context")?;
            if let Some(addr) = context.get_config(Config::ConfiguredAddr).await? {
                println!("{}", addr);
            }
        }
        BotActions::ShowQr => {
            let bot = Bot::new().await.context("Failed to create bot")?;
            match db::get_config(&mut *bot.get_db_connection().await?).await {
                Ok(config) => {
                    qr2term::print_qr(config.invite_qr.clone())?;
                    println!("{}", config.invite_qr);
                }
                Err(_) => println!("Bot not configured yet, start the bot first"),
            }
        }
        BotActions::Version => print!("{}", VERSION),
        BotActions::Start => {
            let mut bot = Bot::new().await.context("Failed to create bot")?;
            bot.start().await.context("Failed to start the bot")?;
            signal::ctrl_c().await?;
        }
    }
    Ok(())
}
